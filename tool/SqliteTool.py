import sqlite3
import uuid
import os


class EasySqlite:
    """
    sqlite数据库操作工具类
    database: 数据库文件地址，例如：db/mydb.db
    """
    _connection = None

    def __init__(self):
        database = r"E:\work\work_custom\easySearch\db\test.db"
        # 连接数据库
        self._connection = sqlite3.connect(database)

    def _dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def execute(self, sql, args=[], result_dict=True, commit=False) -> list:
        """
        执行数据库操作的通用方法
        Args:
        sql: sql语句
        args: sql参数
        result_dict: 操作结果是否用dict格式返回
        commit: 是否提交事务
        Returns:
        list 列表，例如：
        [{'id': 1, 'name': '张三'}, {'id': 2, 'name': '李四'}]
        """
        if result_dict:
            self._connection.row_factory = self._dict_factory
        else:
            self._connection.row_factory = None
        # 获取游标
        _cursor = self._connection.cursor()
        # 执行SQL获取结果
        _cursor.execute(sql, args)
        if commit:
            self._connection.commit()
        data = _cursor.fetchall()
        _cursor.close()
        return data

    # uuid
    def generate_uuid(self):
        uid = str(uuid.uuid1())
        return ''.join(uid.split("-"))


class SearchLink:

    # 查询出所有
    def queryAll(self):
        sql = "select * from tm_link t order by link_usc_count"
        return EasySqlite().execute(sql=sql)

    # 查询出记录条数
    def queryCount(self):
        sql = "select count(*) as count from tm_link"
        return EasySqlite().execute(sql=sql)[0]['count']

    # 分页查询
    def queryByPage(self, name, limit, offset):
        import math

        _count_sql = "select count(*) as count from tm_link where link_name like ?"
        _count_num = EasySqlite().execute(sql=_count_sql, args=["%" + name + "%"])[0]['count']
        _total = math.ceil(_count_num / limit)

        _sql = "select * from tm_link where link_name like ? order by link_usc_count desc limit ? offset ?"

        offset = (offset - 1) * limit
        args = ["%" + name + "%", limit, offset]

        rows = EasySqlite().execute(sql=_sql, args=args)

        _data_ = {"total": _total, "rows": rows}
        return _data_

    # 关键字搜索
    def queryKeyword(self, name):
        return EasySqlite().execute(sql='select * from tm_link t where t.link_name like ? order by link_usc_count desc',
                                    args=["%" + name + "%"])

    # 查询快捷方式是否已存在 存在true
    def queryLinkIsExist(self, name):
        list = EasySqlite().execute(sql='select * from tm_link t where t.link_name =?', args=[name])
        return len(list) > 0

    # 新增快捷方式
    def addLink(self, link_id, link_name, link_path, link_type=1, link_usc_count=0):
        sql = "insert into tm_link('link_id','link_name','link_path','link_type','link_usc_count') values(?,?,?,?,?)"
        EasySqlite().execute(sql=sql, args=[link_id, link_name, link_path, link_type, link_usc_count], commit=True)

    # 增加使用次数
    def addLinkUseCount(self, link_id):
        sql = "update tm_link set link_usc_count = link_usc_count + 1 where link_id = ?"
        EasySqlite().execute(sql=sql, args=[link_id], commit=True)
