import ctypes
import ctypes.wintypes
import win32api

import win32con
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5.QtWidgets import QSystemTrayIcon, QMenu, QAction, QHBoxLayout, QLineEdit, QListWidget, \
    QListWidgetItem, QWidget, QVBoxLayout, QLabel, QDesktopWidget

from tool import SqliteTool

from enum import Enum


# WidgetsItem类型枚举
class WidgetsItemType(Enum):
    # 数据Item
    dataItem = 1
    # 分页Item
    paginationItem = 2

    # 获取name
    @staticmethod
    def get_enum_name(_value):
        _name = ""
        for item in WidgetsItemType:
            if _value == item.value:
                _name = item.name
                break
        return _name

    # 获取value
    @staticmethod
    def get_enum_value(_name):
        _value = ""
        for item in WidgetsItemType:
            if _name == item.name:
                _value = item.value
                break
        return _value


# 搜索框
class Search(QWidget):
    def __init__(self, *args, **kwargs):
        super(Search, self).__init__(*args, **kwargs)
        self.resize(800, 50)

        # FramelessWindowHint 去掉标题栏 WindowStaysOnTopHint 窗口置顶
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)

        # 程序图标
        self.setWindowIcon(QIcon('img/earth.png'))
        # 系统托盘
        self.tray()

        # 窗口出现位置
        self.desktop_location()

        # 上下布局 上面搜索框 下面结果List

        # 上面
        self.searchText = QLineEdit(self)
        self.searchText.setObjectName("searchText")
        self.searchText.textChanged.connect(self.search_text_change)
        self.searchText.returnPressed.connect(self.search_text_enter)
        self.searchText.setTextMargins(10, 0, 0, 0)
        self.searchText.setGeometry(QtCore.QRect(0, 0, 800, 50))
        # 下面
        self.searchResult = QListWidget(self)
        self.searchResult.setObjectName("searchResult")
        self.searchResult.setGeometry(QtCore.QRect(0, 50, 800, 450))
        self.searchResult.itemClicked.connect(self.search_result_click)
        # 搜索结果默认隐藏
        self.searchResult.hide()
        self.show()

    # 搜索框文本change事件
    def search_text_change(self):
        self.resize(800, 500)
        self.searchResult.show()
        self.searchResult.clear()

        text = self.searchText.text()

        self.load_software(text=text, select_first=True)

    # 搜索框回车
    def search_text_enter(self):
        _count = self.searchResult.count()
        if _count > 0:
            _current_item_index = self.searchResult.currentRow()
            if -1 != _current_item_index:
                _current_item = self.searchResult.itemWidget(self.searchResult.currentItem())
                _type = int(_current_item.lb_type.text())

                # 打开软件
                if WidgetsItemType.dataItem.value == _type:
                    _link_id = _current_item.lb_id.text()
                    _link_path = _current_item.lb_subtitle.text()
                    self.open_software(_link_id, _link_path)
                # 点击查看更多
                elif WidgetsItemType.paginationItem.value == _type:
                    # 删除查看更多
                    _lastItemIndex = self.searchResult.count() - 1
                    self.searchResult.takeItem(_lastItemIndex)

                    _text = self.searchText.text()
                    _offset = int(_current_item.lb_offset.text())
                    self.load_software(text=_text, offset=_offset)

    # 搜索结果点击事件
    def search_result_click(self):
        _current_item = self.searchResult.itemWidget(self.searchResult.currentItem())
        _type = int(_current_item.lb_type.text())
        # 打开软件
        if WidgetsItemType.dataItem.value == _type:
            _link_id = _current_item.lb_id.text()
            _link_path = _current_item.lb_subtitle.text()
            self.open_software(_link_id, _link_path)
        # 点击查看更多
        elif WidgetsItemType.paginationItem.value == _type:
            # 删除查看更多
            _lastItemIndex = self.searchResult.count() - 1
            self.searchResult.takeItem(_lastItemIndex)

            _text = self.searchText.text()
            _offset = int(_current_item.lb_offset.text())
            self.load_software(text=_text, offset=_offset)

    # 加载软件列表
    def load_software(self, text, offset=1, select_first=False):
        _limit = 10
        _total = 1
        _list = {}

        if "-all" == text:
            _list = SqliteTool.SearchLink().queryAll()
        elif "" != text:
            _data = SqliteTool.SearchLink().queryByPage(name=text, limit=_limit, offset=offset)
            _list = _data["rows"]
            _total = _data["total"]

        for s in _list:
            item_widget = QListWidgetItem()
            item_widget.setSizeHint(QtCore.QSize(90, 50))
            self.searchResult.addItem(item_widget)
            ico = "img/appIco/" + s["link_id"] + ".ico"
            args = {"type": WidgetsItemType.dataItem.value, "link_id": s["link_id"], "link_name": s["link_name"],
                    "link_path": s["link_path"], "icon_path": ico}
            label = CustomItem(args=args)
            self.searchResult.setItemWidget(item_widget, label)

        if offset < _total:
            # 显示点击查看更多
            item_widget = QListWidgetItem()
            item_widget.setSizeHint(QtCore.QSize(90, 50))
            self.searchResult.addItem(item_widget)
            other = CustomItem(args={"type": WidgetsItemType.paginationItem.value, "offset": offset + 1})
            self.searchResult.setItemWidget(item_widget, other)

        # 自动选中第一条
        if select_first:
            count = self.searchResult.count()
            if count > 0:
                self.searchResult.setCurrentRow(0)

    # 打开软件
    def open_software(self, _link_id, _link_path):
        # 打开软件
        win32api.ShellExecute(0, 'open', _link_path, '', '', 1)

        # 使用次数+1
        SqliteTool.SearchLink().addLinkUseCount(_link_id)

        # 关闭窗口 清除搜索信息
        self.searchResult.clear()
        self.searchResult.hide()
        self.searchText.clear()
        self.hide()
        self.resize(800, 50)

    # 窗口出现位置
    def desktop_location(self):
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2,
                  (screen.height() - size.height()) / 3)

    # 重写键盘事件
    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Up:
            self.item_cursor_up()
        elif e.key() == QtCore.Qt.Key_Down:
            self.item_cursor_down()

    # 软件列表选中上一个
    def item_cursor_up(self):
        # 当前选中item下标
        _current_item_index = self.searchResult.currentRow()
        if 0 != _current_item_index:
            self.searchResult.setCurrentRow(_current_item_index - 1)

    # 软件列表选中下一个
    def item_cursor_down(self):
        # 当前选中item下标
        _current_item_index = self.searchResult.currentRow()
        # 当前item总数
        _current_item_count = self.searchResult.count()
        if _current_item_index != (_current_item_count - 1):
            self.searchResult.setCurrentRow(_current_item_index + 1)

    # 自定义全局快捷键
    def search_hotkey(self):
        user32 = ctypes.windll.user32  # 加载user32.dll

        # 热键
        hotkey_id = 250

        if not user32.RegisterHotKey(None, hotkey_id, win32con.MOD_ALT,
                                     0x47):  # 注册快捷键ALT+G并判断是否成功，该热键用于执行一次需要执行的内容。
            print("Unable to register id", hotkey_id)  # 返回一个错误信息

        # 以下为检测热键是否被按下，并在最后释放快捷键
        try:
            msg = ctypes.wintypes.MSG()

            while True:
                if user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:

                    if msg.message == win32con.WM_HOTKEY:
                        if msg.wParam == hotkey_id:
                            if self.isVisible():
                                self.hide()
                            else:
                                self.show()
                                self.searchText.setFocus()

                    user32.TranslateMessage(ctypes.byref(msg))
                    user32.DispatchMessageA(ctypes.byref(msg))

        finally:
            # 必须得释放热键，否则下次就会注册失败，所以当程序异常退出，没有释放热键，
            # 那么下次很可能就没办法注册成功了，这时可以换一个热键测试
            user32.UnregisterHotKey(None, hotkey_id)

    # 系统托盘
    def tray(self):
        self.tray = QSystemTrayIcon()
        icon = QIcon("img/earth.png")
        self.tray.setIcon(icon)

        menu = QMenu()
        showAction = QAction("显示主窗口", self, triggered=self.search_quit)
        settingAction = QAction("设置", self, triggered=self.search_quit)
        quitAction = QAction("退出", self, triggered=self.search_quit)

        menu.addAction(showAction)
        menu.addAction(settingAction)
        menu.addAction(quitAction)
        self.tray.setContextMenu(menu)

    # 关闭程序
    @staticmethod
    def search_quit():
        sys.exit()


# 自定义 Item
class CustomItem(QWidget):
    def __init__(self, args={}):
        super(CustomItem, self).__init__()

        _item_type = args['type']
        self.lb_type = QLabel(str(_item_type))
        self.lb_type.hide()

        # 数据item
        if _item_type == WidgetsItemType.dataItem.value:
            _link_id = args["link_id"]
            _link_name = args["link_name"]
            _link_path = args["link_path"]
            _icon_path = args["icon_path"]

            self.lb_icon = QLabel()
            self.lb_icon.setFixedSize(32, 32)
            pix_map = QPixmap(_icon_path).scaled(self.lb_icon.width(), self.lb_icon.height())
            self.lb_icon.setPixmap(pix_map)

            self.lb_title = QLabel(_link_name)
            self.lb_title.setFont(QFont("Arial", 10, QFont.Bold))

            self.lb_subtitle = QLabel(_link_path)
            self.lb_subtitle.setFont(QFont("Arial", 8, QFont.StyleItalic))

            self.lb_id = QLabel(_link_id)
            self.lb_id.hide()

            self.generate_data_item_ui()
        # 显示更多Item
        elif _item_type == WidgetsItemType.paginationItem.value:
            _offset = args["offset"]

            self.other = QLabel("显示更多···")
            self.other.setFont(QFont("Arial", 9, QFont.Black))
            self.other.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

            self.lb_offset = QLabel(str(_offset))
            self.lb_offset.hide()

            self.generate_page_item()

    # 生成数据item
    def generate_data_item_ui(self):
        # 左右布局 左边图标 右边上下布局 标题和父标签
        _main_layout = QHBoxLayout(self)

        # 左边 图标
        _main_layout.addWidget(self.lb_icon)

        # 右边 标题和副标题
        _right_layout = QVBoxLayout(self)
        _right_layout.addWidget(self.lb_title)
        _right_layout.addWidget(self.lb_subtitle)

        _main_layout.addLayout(_right_layout)

        self.setLayout(_main_layout)

    # 生成提示还有更多数据item
    def generate_page_item(self):
        _main_layout = QHBoxLayout(self)
        _main_layout.addWidget(self.other)
        self.setLayout(_main_layout)


# 美化样式表
Stylesheet = """
    QLineEdit#searchText{
        border-style:outset;
        background-color:#3E4143;
        font-size:30px;
        color:#E5E5E6;
    }
    QListWidget#searchResult{
        border-style:outset;
    }
"""

if __name__ == "__main__":
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    w = Search()
    w.setStyleSheet(Stylesheet)
    w.search_hotkey()
    sys.exit(app.exec_())
